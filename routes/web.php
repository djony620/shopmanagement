<?php
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('auth.login');
//});

Auth::routes();

Route::get('/', function (){
    return redirect(route('login'));
});
Route::get('/sign/out', function (){
    Auth::logout();
    return redirect(route('login'));
})->name('sign_out');
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('/products', 'HomeController@products')->name('products');
Route::get('/sells', 'HomeController@Sells')->name('Sells');
Route::get('add/product-{id}', 'HomeController@AddProducts')->name('AddProducts');
Route::get('add/product', 'HomeController@AddProducts');
Route::post('product/save', 'HomeController@saveProduct')->name('saveProduct');
Route::get('product/delete-{id}', 'HomeController@productDelete')->name('productDelete');
Route::get('start/sell', 'ProductSellController@productSellStart')->name('productSellStart');
Route::get('start/sell/search', 'ProductSellController@productsSellStart')->name('productsSellStart');
Route::post('make/sell/report', 'ProductSellController@makeSellReport')->name('makeSellReport');
Route::get('import/product/excel', 'ProductSellController@importExcel')->name('importExcel');
Route::post('import/product/save', 'ProductSellController@importSave')->name('importSave');
