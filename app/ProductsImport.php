<?php

namespace App\Imports;

use App\Product;
use Maatwebsite\Excel\Concerns\ToModel;

class ProductsImport implements ToModel
{

    public function model(array $row)
    {
        return new Product([
            'title'     => $row[0],
            'photo'    => strtolower(str_replace(' ', '', $row[0])).'.jpg',
            'quantity'    => $row[1],
            'sell_price'    => $row[2],
            'buy_price'    => $row[3],
            'discount'    => $row[4],
            'discount_type'    => $row[5],
            'stock'    => $row[6],
            'product_code'    => $row[7],
        ]);

    }
}