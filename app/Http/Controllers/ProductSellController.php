<?php

namespace App\Http\Controllers;

use App\Imports\ProductsImport;
use App\Product;
use App\Sale;
use App\SellId;
use Illuminate\Contracts\Validation\ImplicitRule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Excel;

class ProductSellController extends Controller
{
    private $excel;

    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }
    public function productSellStart()
    {
        Session::forget('checked_item');
        return view('start_selling');
    }

    public function productsSellStart(Request $request)
    {

        if ($request->ajax()) {
            $products = Product::all();

            return datatables($products)
                ->addColumn('photo', function ($item) {

                    return photo_html($item->photo, 100, 100);
                })
                ->addColumn('choose', function ($item) {
                    $checked = "";
                 $html = '';
                 if($item->stock > 0){
                     if(!empty(session('checked_item')) && in_array($item->id,session('checked_item')))
                         $checked = "checked";
                     $html .= '<div class="kas-checks-squaredTwo">
                <input type="checkbox" name="check[]" value="' . $item->id . '" '.$checked.' class="choose" id="check_' . $item->id . '">
                <label for="check_' . $item->id . '"></label>
                </div>';
                 }else{
                     $html = '<strong style="color: red">No stock</strong>';
                 }
                    return $html;
                })
                ->rawColumns(['choose', 'photo'])
                ->make(true);

        }
    }

    public function makeSellReport(Request $request){

        $products = [];
        if(isset($request->sell_ids[0]))
        $products = Product::whereIn('id',$request->sell_ids)->get();
         $all_request = $request->all();

        session(['checked_item' => $request->sell_ids]);
        $html = "";
        if(isset($products[0])){
            $total = 0;
            foreach ($products as $product){
                $quanity = 'quantity_'.$product->id;
                $value = (isset($request->$quanity) && !empty($request->$quanity))? $request->$quanity : 1;
                $html .= "<tr>";
                $html .= "<td >".str_limit($product->title,20)."</td>";
                $html .= "<td >".$product->sell_price."</td>";
                $html .= "<td >".$product->stock."</td>";
                $html .= "<td ><input class='receipt' onkeypress=\"return isNumberKey(event)\" data-quantity='".$product->stock."' type='text' name='quantity_".$product->id."' value='".$value."' /></td>";
                $html .= "<td >".$product->sell_price*$value."</td>";
                $html .="</tr>";

                $total+=$product->sell_price*$value;

            }
            $html .= "<tr>";
            $html .= "<td colspan='4' >Total</td>";
            $html .= "<td >".$total."</td>";
            $html .="</tr>";
            $html .= "<tr>";
            $html .= "<td colspan='4' ></td>";
            $html .= "<td ><button type='button' onclick='print()' >Money receipt</button></td>";
            $html .="</tr>";
        }
        if(isset($request->type) && $request->type == 'print'){
            Session::forget('checked_item');
            $sell_id = SellId::create();
            $html = "";
            $data['all_request'] = $request->all();
            $data['products'] = $products;
            $html .=View::make('pdf_and_print.money_receipt',$data);
            if(isset($products[0])){
                foreach ($products as $product){
                    $q = 'quantity_'.$product->id;
                    $sele['sell_id'] = $sell_id->id;
                    $sele['product_id'] = $product->id;
                    $sele['quantity'] = $request->$q;
                    $sele['discount'] = 0;
                    $sele['buy_price'] = $product->buy_price;
                    $sele['sell_price'] = $product->sell_price;
                    Sale::create($sele);
                    $product_data = Product::find($product->id);

                    $product_data->update(['stock'=>$product->stock - $request->$q]);
                }
            }

            return response()->json(['data'=>$html]);
        }

        return response()->json(['data'=>$html]);
    }
// excel upload
public function importExcel(){
        return view('excel');
}
public function importSave(Request $request){

   try{
       $validatedData = Validator::make($request->all(),[
           'excel' => 'required|mimes:xlsx',
           'photos' => 'required|array',
       ]);
       if ($validatedData->fails()) {
           return back()->withErrors($validatedData)->withInput($request->input());
       }


       $this->excel->import(new ProductsImport, $request->file('excel'));
       if(isset($request->photos[0])){
           $photos = $request->photos;
           $photos_name_array = [];
           if(isset($photos[0])){
               foreach ($photos as $photo){
                   //  dd(Product::where('photo',$photo->getClientOriginalName())->exists(),$photo->getClientOriginalName());
                   if(Product::where('photo',$photo->getClientOriginalName())->exists()){
                       uploadimageForXl($photo,'img/');
                   }
               }
           }

       }

       return redirect()->back()->with('dismiss','Product import successfully');
   }catch (\Exception $exception){
       return redirect()->back()->with('dismiss','Something went wrong');
   }
}

}
