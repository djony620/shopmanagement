<?php

namespace App\Http\Controllers;

use App\Product;
use App\Sale;
use App\SellId;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpSpreadsheet\Worksheet\Row;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {




        if($request->ajax()){
            $charts = Sale::select(
                DB::raw('DATE(created_at) as date')
                ,DB::raw('SUM(sell_price) as sell')
                ,DB::raw('SUM(buy_price) as buy')
            )->groupBy('date')->orderBy('date','asc')->take(30)->get();
            if (isset($charts[0])){
                foreach ($charts as $chart){
                    $chart['visits'] = $chart->sell - $chart->buy;
                    $chart['country'] = date('dS M',strtotime($chart->date));
                }
            }

           return response()->json([$charts]);
        }
        $data['total_products'] = Product::count();
        $data['available_products'] = Product::where('stock','!=',0)->count();
        $data['unavailable_products'] = Product::where('stock','=',0)->count();
        $data['total_customers'] = SellId::count();
        $data['today_total_sales'] = Sale::whereDate('created_at',date('Y-m-d'))->sum('sell_price');
        $data['today_total_buy'] = Sale::whereDate('created_at',date('Y-m-d'))->sum('buy_price');
        $data['today_total_profit'] = $data['today_total_sales'] - $data['today_total_buy'];
      //  $data['total_profit'] = $data['total_sales'] - $data['total_buy'];
//dd($data,date('Y-m-d'));
        return view('dashboard');
    }
    public function products(Request $request){

        if($request->ajax()){
            $products = Product::all();

            return datatables($products)
                ->addColumn('photo',function ($item){

                    return photo_html($item->photo,100,100);
                })
                ->addColumn('actions',function ($item){
                    return '<ul class="d-flex activity-menu">
                         
                         '.edit_html('AddProducts',$item->id).'
                         '.delete_html('productDelete',$item->id).'
                       
                        </ul>';
                })
                ->rawColumns(['actions','photo'])
                ->make(true);

        }
        return view('products');
    }
    public function AddProducts($id = null){
        $data['item'] = '';
        if(!empty($id))
        $data['item'] = Product::find($id);

        return view('addEditProduct',$data);
    }
    public function saveProduct(Request $request){
dd($request->all());
        $validatedData = Validator::make($request->all(),[
            'title' => 'required|max:255',
            'quantity' => 'required|numeric',
            'buy_price' => 'required|numeric',
            'sell_price' => 'required|numeric|min:'.$request->buy_price,
            'stock' => 'required|numeric',
            'product_code' => 'required|unique:products,product_code,'.(isset($request->id)?$request->id:''),
        ]);
        if ($validatedData->fails()) {
            return back()->withErrors($validatedData)->withInput($request->input());
        }
        $product = '';
        if(!empty($request->id)){
            $product = Product::find($request->id);
        }


        $data['title'] = $request->title;
        $data['quantity'] = $request->quantity;
        $data['buy_price'] = $request->buy_price;
        $data['sell_price'] = $request->sell_price;
        $data['stock'] = $request->stock;
        if(!empty($request->file('photo'))){
            $data['photo'] = uploadimage($request->file('photo'),'img/',(!empty($product)?$product->photo:""),'');
        }

        $data['product_code'] = $request->product_code;

        if(!empty($product->id)){
            $product->update($data);
            $json['error'] = false;
            $message = 'Product update successfully';
        }else{
            Product::create($data);
            $json['error'] = false;
            $message = 'Product add successfully';
        }

       return redirect()->back()->with('message',$message);


    }

    public function productDelete($id){
       $dlt = Product::find($id)->delete();
       return redirect()->back()->with('message','Product delete successfully');
    }

    public function Sells(Request $request){
        if($request->ajax()){

            $products = Sale::join('products','products.id','sales.product_id')
                ->select(
                    'products.photo'

                    ,'sales.sell_id'
                    ,DB::raw('GROUP_CONCAT(products.photo) as photos')
                    ,DB::raw('GROUP_CONCAT(products.product_code) as data_product_code')
                    ,DB::raw('GROUP_CONCAT(products.title) as data_title')
                    ,DB::raw('GROUP_CONCAT(products.buy_price) as buy_price')
                    ,DB::raw('GROUP_CONCAT(products.sell_price) as sell_price')
                    ,'sales.created_at'
                )
                ->groupBy('sales.sell_id');

            return datatables($products)
                ->addColumn('product_code',function ($item){
                    return $item->data_product_code;
                })->addColumn('title',function ($item){
                    return $item->data_title;
                })
                ->addColumn('total_buy',function ($item){
                   $buy = 0;
                   if(!empty($item->buy_price)){
                       $buy_prices = explode(',',$item->buy_price);
                       if(isset($buy_prices[0])){
                           foreach ($buy_prices as $buy_price){
                               $buy +=$buy_price ;
                           }
                       }
                   }
                    return $buy;
                })->addColumn('total_sell',function ($item){
                   $sell = 0;
                   if(!empty($item->sell_price)){
                       $sell_prices = explode(',',$item->sell_price);
                       if(isset($sell_prices[0])){
                           foreach ($sell_prices as $sell_price){
                               $sell +=$sell_price;
                           }
                       }
                   }
                    return $sell;
                })->addColumn('profit',function ($item){
                    $sell = 0;
                    $buy = 0;
                    if(!empty($item->sell_price)){
                        $sell_prices = explode(',',$item->sell_price);
                        if(isset($sell_prices[0])){
                            foreach ($sell_prices as $sell_price){
                                $sell += $sell_price;
                            }
                        }
                    }

                    if(!empty($item->buy_price)){
                        $buy_prices = explode(',',$item->buy_price);
                        if(isset($buy_prices[0])){
                            foreach ($buy_prices as $buy_price){
                                $buy +=$buy_price ;
                            }
                        }
                    }

                     return $sell-$buy;

                })

                ->rawColumns(['total_buy','total_sell','profit'])
                ->make(true);

        }
        return view('sell_list');
    }
}
