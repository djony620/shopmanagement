<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{

    protected $fillable = [
        'id','sell_id','product_id', 'quantity','discount','buy_price','sell_price','created_at'
    ];
}
