<?php

function uploadimage($img, $path, $user_file_name = null, $width = null, $height = null)
{

    if (!file_exists($path)) {
        mkdir($path, 0777, true);
    }

    if (isset($user_file_name) && $user_file_name != "" && file_exists($path . $user_file_name)) {

        unlink(public_path().'/'.$path . $user_file_name);
    }

    // saving image in target path
    $imgName = uniqid() . '.' . $img->extension();
    $imgPath = public_path($path . $imgName);
    // making image

    if (move_uploaded_file($img, $imgPath)) {
        return $imgName;
    }
    return false;
}


function uploadimageForXl($img, $path, $user_file_name = null, $width = null, $height = null)
{

    if (!file_exists($path)) {
        mkdir($path, 0777, true);
    }

    if (isset($user_file_name) && $user_file_name != "" && file_exists($path . $user_file_name)) {

        unlink(public_path().'/'.$path . $user_file_name);
    }

    // saving image in target path
    $imgName = $img->getClientOriginalName();

    $imgPath = public_path($path . $imgName);
    // making image

    if (move_uploaded_file($img, $imgPath)) {
        return $imgName;
    }
    return false;
}


function delete_html($route,$id){
    $html='<li style="display: inline" class="deleteuser"><a href="#delete_'.$id.'" data-toggle="modal"><i class="fa fa-trash-alt"></i></a> <span>' . __('Delete') . '</span></li>';
    $html .='<div id="delete_'.$id. '" class="modal fade delete" role="dialog">';
    $html .='<div class="modal-dialog modal-sm">';
    $html .='<div class="modal-content">';
    $html .='<div class="modal-header"><h6 class="modal-title">'.__('Delete').'</h6><button type="button" class="close" data-dismiss="modal">&times;</button></div>';
    $html .='<div class="modal-body"><p>'.__('Would you want to delete ?').'</p></div>';
    $html .='<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">'.__("Close").'</button>';
    $html .='<a class="btn delete-btn"href="' . route($route, $id) . '">' . __('Confirm') . '</a>';
    $html .='</div>';
    $html .='</div>';
    $html .='</div>';
    $html .='</div>';
    return $html;
}

function edit_html($route,$id){
    $html='<li style="display: inline" class="viewuser"><a href="' . route($route, $id) . '"><i class="far fa-edit"></i></a> <span>' . __('Edit') . '</span></li>';
    return $html;
}

function photo_html($img,$width,$higth){

     $src = asset('img/default.jpg');
    if(!empty($img) && public_path().'/img/'.$img){
        $src = asset('img/'.$img);
    }
    return '<img src="'.$src.'"  style="width:'.$width.'px;height:'.$higth.'px" />';
}

function printReceipt($products,$all_request){
    dd($products,$all_request);
}

function filemanager($array){

    $html = '';
    $html .= "<span>
              <img onclick='openGallery()' id='file_select' src='".$array['default_image']."' width='".$array['image_width']."' height='".$array['image_height']."'  class='' data-toggle='modal' data-target='' alt='' >
              </span>";

    $html .="<div class='modal fade' id=galleryModal tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
          <div class='modal-dialog modal-lg' role='document'>
            <div class='modal-content'>
              <div class='modal-header'>
                <h5 class='modal-title' id='exampleModalLabel'>Modal title</h5>
                <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                  <span aria-hidden='true'>&times;</span>
                </button>
              </div>
              <div class='modal-body' id='iframe_body' style='min-height: 444px;'>
                <iframe id='iframe' name='ifreme'  style='width: 100%;height: 444px' frameborder='0'
                                     src=".asset('file_manager/filemanager/dialog.php?type=1&amp;field_id=fieldID&amp;relative_url=1&amp;multiple=1&public_path='.$array['public_path'].'&asset='.$array['asset']).">
                            </iframe>
              </div>
              <div class='modal-footer'>
                <button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>
                <button type='button' onclick='dxxxxzsdsdd()' id='TakeSelectedImage' class='btn btn-primary'>Save changes</button>
              </div>
            </div>
          </div>
        </div>";

    $html.="
             <script>
             function responsive_filemanager_callback(){
               console.log('s');
                  $('#galleryModal').removeClass('in');
                  $('.modal-backdrop').remove();
                  $('body').removeClass('modal-open');
                 
                  $('#galleryModal').hide();
             }
             
             function openGallery() {
                  $('#galleryModal').addClass('in');
                  $('.modal-backdrop').add();
                  $('body').addClass('modal-open');
             
                  $('#galleryModal').show();
             }
            </script> 
           ";


    return  $html;
}


