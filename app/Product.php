<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = [
        'id', 'title', 'photo','quantity','sell_price','buy_price','discount','discount_type','stock','product_code',
    ];
}
