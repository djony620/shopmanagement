@extends('layouts.master',['main_menu'=>'excel'])
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Upload Products by EXCEL</h3>
                    </div>

                    {!! Form::open(['url' => route('importSave'),'id'=>'','files'=>true]) !!}
                    <div class="box-body">
                        <!-- Color Picker -->
                        <div class="form-group">
                            <label>emaple File: <a href=""><strong>click here</strong> </a>to download</label>
                        </div>
                        <div class="form-group">
                            <label>Excel File:</label>
                            <input type="file" name="excel" class="form-control my-colorpicker1 colorpicker-element">
                            <span class="error">{{ $errors->first('excel') }}</span>
                        </div>
                        <div class="form-group">
                            <label>Photos(<strong>Photo name must be format (product tile.jpg).[WITHOUT SPACE]</strong>):</label>
                            <input type="file" name="photos[]" multiple="multiple" class="form-control my-colorpicker1 colorpicker-element">
                            <span class="error">{{ $errors->first('photos') }}</span>
                        </div>

                        <button type="submit" class="btn btn-default pull-right">Submit</button>
                        <!-- time Picker -->

                    </div>
                    {!! Form::Close() !!}
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
@endsection
