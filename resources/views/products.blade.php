@extends('layouts.master',['main_menu'=>'products'])
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-md-3">
                                <h3 class="box-title">Products</h3>
                            </div>
                            <div class="col-md-9 pull-right">
                                <a class="btn btn-default" href="{{url('add/product')}}">Add</a>
                            </div>
                        </div>


                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="products" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Photo</th>
                                <th>Product code</th>
                                <th>Name</th>
                                <th>Buy price</th>
                                <th>Sell price</th>
                                <th>Stock</th>
                                <th>Action</th>

                            </tr>
                            </thead>


                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('script')
    <script>
        $('#products').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 25,
            responsive: true,
            ajax: '{{route('products')}}',
            order: [6, 'desc'],
            autoWidth:true,
            columns: [
                {"data": "photo"},
                {"data": "product_code"},
                {"data": "title"},
                {"data": "buy_price"},
                {"data": "sell_price"},
                {"data": "stock"},
                {"data": "actions",orderable: false, searchable: false}
            ]
        });
    </script>
    @endsection