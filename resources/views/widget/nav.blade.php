<nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                 @if(!empty(\Illuminate\Support\Facades\Auth::user()->photo))
                        <img src="{{\Illuminate\Support\Facades\Auth::user()->photo}} " class="user-image" alt="User Image">
                    @else

                        <img src="{{asset('img/default/man.png')}}" class="user-image" alt="User Image">
                    @endif

                    <span class="hidden-xs">{{\Illuminate\Support\Facades\Auth::user()->name}}</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <img src="
@if(!empty(\Illuminate\Support\Facades\Auth::user()->photo))
                           {{\Illuminate\Support\Facades\Auth::user()->photo}}
                        @else
                        {{asset('img/default/man.png')}}
                        @endif
" class="img-circle" alt="User Image">

                        <p>
                            {{\Illuminate\Support\Facades\Auth::user()->name}}

                        </p>
                    </li>
                    <!-- Menu Body -->

                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                            <a href="{{route('sign_out')}}" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                    </li>
                </ul>
            </li>
            <!-- Control Sidebar Toggle Button -->
            <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
            </li>
        </ul>
    </div>
</nav>