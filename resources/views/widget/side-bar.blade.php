<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">

                @if(!empty(\Illuminate\Support\Facades\Auth::user()->photo))
                    <img src="{{\Illuminate\Support\Facades\Auth::user()->photo}} " class="user-image" alt="User Image">
                @else

                    <img src="{{asset('img/default/man.png')}}" class="user-image" alt="User Image">
                @endif

            </div>
            <div class="pull-left info">
                <p>{{\Illuminate\Support\Facades\Auth::user()->name}}</p>
                <a href="{{route('dashboard')}}"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MENU</li>
            <li class="treeview">
            <li class="{{(!empty($main_menu) && $main_menu=='dashboard')? "active":'' }}" ><a href="{{route('dashboard')}}" ><a href="{{route('dashboard')}}"><i class="fa fa-circle-o"></i>Dashboard</a></li>
            <li class="{{(!empty($main_menu) && $main_menu=='start_sell')? "active":'' }}" ><a href="{{route('productSellStart')}}"><i class="fa fa-circle-o"></i>Start selling</a></li>
            <li class="{{(!empty($main_menu) && $main_menu=='Sells')? "active":'' }}"><a href="{{route('Sells')}}"><i class="fa fa-circle-o"></i>Sells</a></li>

            <li class="{{(!empty($main_menu) && $main_menu=='products')? "active":'' }}" ><a href="{{route('products')}}"><i class="fa fa-circle-o"></i>Products</a></li>
            <li class="{{(!empty($main_menu) && $main_menu=='excel')? "active":'' }}" ><a  href="{{route('importExcel')}}"><i class="fa fa-circle-o"></i>Import product By Xl</a></li>
            <li><a href="{{route('dashboard')}}"><i class="fa fa-circle-o"></i>Invoices</a></li>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>