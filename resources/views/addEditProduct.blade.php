
@extends('layouts.master',['main_menu'=>'products'])
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.0/jquery.fancybox.css" />

    <section class="content">
        <div class="col-md-12">
            <div class="row">
                <!-- left column -->

                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add product</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::open(['url' => route('saveProduct'),'id'=>'saveProduct','files'=>true]) !!}
                    @if(!empty($item))
                    <input type="hidden" name="id" value="{{$item->id}}">
                    @endif
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Title</label>
                                        <input type="text"  name="title" value="{{old('title',(isset($item->title))? $item->title :'')}}" class="form-control" id="name" placeholder="Enter Title">
                                        <span class="error">{{ $errors->first('title') }}</span>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Buy price</label>
                                        <input type="text" name="buy_price" class="form-control" value="{{old('buy_price',(isset($item->buy_price))? $item->buy_price :'')}}" id="exampleInputPassword1" placeholder="Enter Quantity">
                                        <span class="error">{{ $errors->first('buy_price') }}</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Sell price</label>
                                        <input type="text" name="sell_price" class="form-control" value="{{old('sell_price',(isset($item->sell_price))? $item->sell_price :'')}}" id="exampleInputPassword1" placeholder="Sell Quantity">
                                        <span class="error">{{ $errors->first('sell_price') }}</span>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Discount</label>
                                        <input type="text" name="discount" class="form-control" value="{{old('discount',(isset($item->discount))? $item->discount :'')}}" id="exampleInputPassword1" placeholder="Discount">
                                        <span class="error">{{ $errors->first('discount') }}</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Discount type</label>
                                        <select name="discount_type" id="" class="form-control">
                                            <option @if(old('discount_type',(isset($item->discount_type))? $item->discount_type :'') == '') selected @endif value="">Select</option>
                                            <option @if(old('discount_type',(isset($item->discount_type))? $item->discount_type :'') == 1) selected @endif  value="1">Normal</option>
                                            <option @if(old('discount_type',(isset($item->discount_type))? $item->discount_type :'') == 2) selected @endif  value="2">Parsentege</option>
                                        </select>
                                        <span class="error">{{ $errors->first('discount_type') }}</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Stock</label>
                                        <input type="number" name="stock" class="form-control" value="{{old('stock',(isset($item->stock))? $item->stock :'')}}"  id="exampleInputPassword1" placeholder="Stock">
                                        <span class="error">{{ $errors->first('stock') }}</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Product code</label>
                                        <input type="text" name="product_code" class="form-control" value="{{old('product_code',(isset($item->product_code))? $item->product_code :'')}}" id="exampleInputPassword1" placeholder="Product code">
                                        <span class="error">{{ $errors->first('product_code') }}</span>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Quantity</label>
                                        <input type="text" name="quantity" class="form-control" value="{{old('quantity',(isset($item->quantity))? $item->quantity :'')}}" id="exampleInputPassword1" placeholder="Enter Quantity">
                                        <span class="error">{{ $errors->first('quantity') }}</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputFile">Photo</label>
                                                <input type="file" name="photo"  id="exampleInputFile" onchange="readURL(this,'product_image')">
                                                <span class="error">{{ $errors->first('photo') }}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <img style="width: 100px;height: 100px"  id="product_image" src="{{(!empty($item))?asset('img/'.$item->photo):asset("img/default.jpg")}}"  class="upload_image pull-right" alt="img">
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>


                        </div>

                    {{--<iframe  width="765" height="550" frameborder="0"
                             src="{{asset('file_manager/filemanager/dialog.php?type=0')}}">
                    </iframe>--}}

                    @php
                    $data = [
                    'default_image' => asset('img/default.jpg'),
                    'public_path' => public_path('file_manager'),
                    'asset' => asset('file_manager/'),
                    'image_width' => 100,
                    'image_height' => 100,
                    ];
                    @endphp

                    {!! filemanager($data) !!}

                    <div class="input-append">
                        <input id="fieldID" type="text" value="">
                        <a class="btn iframe-btn fancy" data-fancybox-type="iframe" href="{{asset('file_manager/filemanager/dialog.php?type=1&amp;field_id=fieldID&amp;relative_url=1&amp;multiple=1')}}" >Select</a>
                    </div>
                    <a href="{{asset('file_manager/filemanager/js/tinymce/plugins/filemanager/dialog.php?type=0')}}" class="btn iframe-btn" type="button">Open Filemanager</a>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" id="saveBtn" class="btn btn-primary">Submit</button>
                        </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.box -->

                <!-- /.box -->


                <!--/.col (left) -->

            </div>
        </div>

    </section>
@endsection
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.0/jquery.fancybox.js"></script>
    <script>
        $(document).ready(function () {
            $.get("{{asset('file_manager/filemanager/dialog.php?type=1&amp;field_id=fieldID&amp;relative_url=1&amp;multiple=1')}}").success(function(data){

            });

        });


    </script>
    <script>
        function responsive_filemanager_callback(){
            console.log('ccvcvc');

            $("#galleryModal").hide();
        }
    </script>

@endsection

