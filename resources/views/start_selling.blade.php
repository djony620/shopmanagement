@extends('layouts.master',['main_menu'=>'start_sell'])
<link rel="stylesheet" href="  https://printjs-4de6.kxcdn.com/print.min.css">
@section('content')
    <section class="content">
        <!-- Main content -->
        <section class="content">

            <!-- SELECT2 EXAMPLE -->

            <!-- /.box -->

            <div class="row">
                <div class="col-md-6">
                    <table id="products" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Photo</th>
                            <th>Product code</th>
                            <th>Name</th>
                            <th>Buy price</th>
                            <th>Sell price</th>
                            <th>Stock</th>
                            <th>Choose</th>
                        </tr>
                        </thead>


                    </table>
                </div>
                {!! Form::open(['url' => route('makeSellReport'),'id'=>'sellReport','files'=>true]) !!}
                <div class="col-md-6">
                    <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                        <thead id="sell_ids" class="hidden">
                        <tr role="row">
                            <th>Title</th>
                            <th>sell price</th>
                            <th>stock</th>
                            <th>quantity</th>
                            <th>Total</th>


                        </thead>


                       {{-- <tr role="row" class="odd">
                            <td class="sorting_1">Webkit</td>
                            <td>Safari 1.2</td>
                            <td>OSX.3</td>
                            <td>OSX.3</td>
                            <td>125.5</td>

                        </tr>--}}
                        <tbody id="sell_data">

                        </tbody>


                    </table>
                    <div class="d-none" style="" id="print_data"></div>
                </div>
                <!-- /.col (right) -->
            </div>
        {!! Form::close() !!}
            <!-- /.row -->

        </section>

    </section>
@endsection
@section('script')
    <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
    <script>
        function dataTableData() {

            //$('#products').dataTable().fnDestroy();
       var  ajaxCall =  $('#products').DataTable({

               processing: true,
               serverSide: true,
               pageLength: 25,
               responsive: true,
               destroy: true,
               paging : true,
               ajax: '{{route('productsSellStart')}}',
               order: [6, 'desc'],
               autoWidth:true,
               columns: [
                  {"data": "photo"},
                  {"data": "product_code"},
                  {"data": "title"},
                  {"data": "buy_price"},
                  {"data": "sell_price"},
                  {"data": "stock"},
                  {"data": "choose",orderable: true, searchable: true}
              ]
          });

        return ajaxCall;
        }
        dataTableData();
    </script>
    <script>
        $(document.body).on('click','.choose',function () {
             $('.hidden').removeClass('hidden');
             var choose_value = $(this).val();
            if ($(this).prop('checked')) {
                var sell_ids = $('#sell_ids').append('<input type="hidden" class="sell_ids" name="sell_ids[]" value="'+$(this).val()+'" />');
            }else {
                $('.sell_ids').each(function(index,element){
                    if($(element).val() === choose_value){
                       $(element).remove();
                    }
                });
            }
                var form = $('#sellReport');
                var url = form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(), // serializes the form's elements.
                    success: function(data)
                    {   $('#sell_data').empty();
                        $('#sell_data').append(data.data);
                        if(data.data === '')
                        $('#sell_ids').addClass('hidden');
                    }
                });
        });
    </script>
    <script>
        function print(e) {
            var form = $('#sellReport');
            var url = form.attr('action')+'?type=print';

            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                success: function(data)
                {

                     $('#print_data').append(data.data);
                     printJS({ printable: 'print_data', type: 'html' })
                     $('#print_data').empty();
                    // $('#sell_data').empty();
                    // $('.sell_ids').remove();
                    // $('#sell_ids').addClass('hidden');
                    location.reload();


                }
            });

        }

    </script>
    <script>
        $(document.body).on('input','.receipt',function () {
             if($(this).val() > $(this).data('quantity')){
                 $(this).val(1);
                 $(this).css('border-color','red');
                 return false
             }

            var form = $('#sellReport');
            var url = form.attr('action');
            setTimeout(function () {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(), // serializes the form's elements.
                    success: function(data)
                    {   $('#sell_data').empty();
                        $('#sell_data').append(data.data);

                    }
                });
            },1500);
        });
    </script>
@endsection