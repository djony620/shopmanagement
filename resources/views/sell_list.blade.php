@extends('layouts.master',['main_menu'=>'Sells'])
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Products</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="products" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Sell ID</th>
                                <th>Product code</th>
                                <th>Name</th>
                                <th>Buy price</th>
                                <th>Sell price</th>
                                <th>Total buy price</th>
                                <th>Total sell price</th>
                                <th>Profit</th>
                                <th>Sell date</th>
                            </tr>
                            </thead>


                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('script')
    <script>
        $('#products').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 25,
            responsive: true,
            ajax: '{{route('Sells')}}',
            order: [5, 'desc'],
            autoWidth:true,
            columns: [
                {"data": "sell_id"},
                {"data": "product_code"},
                {"data": "title"},
                {"data": "buy_price"},
                {"data": "sell_price"},
                {"data": "total_buy"},
                {"data": "total_sell"},
                {"data": "profit"},
                {"data": "created_at"},
            ]
        });
    </script>
@endsection