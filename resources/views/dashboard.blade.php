@extends('layouts.master',['main_menu'=>'dashboard'])
@section('content')
    <section class="content">


        <div class="row">
            <div class="col-md-12">
                <!-- AREA CHART -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Products details</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>{{\App\Product::all()->count()}}</h3>

                                    <p>Total Products</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-cubes" aria-hidden="true"></i>
                                </div>

                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>{{\App\Product::all()->where('stock','!=',0)->count()}}</h3>

                                    <p>Available Products</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-cubes" aria-hidden="true"></i>
                                </div>

                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>{{\App\Product::all()->where('stock','=',0)->count()}}</h3>

                                    <p>Unavailable Products</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-cubes" aria-hidden="true"></i>
                                </div>

                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>{{\App\SellId::all()->count()}}</h3>

                                    <p>Total Customer</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Today Sell & Profit</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="col-lg-6 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-aqua">
                                        <div class="inner">
                                            <h3>{{\App\Sale::whereDate('created_at',\Carbon\Carbon::today())->count()}}</h3>

                                            <p>Total Sell</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-dollar"></i>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-6 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-yellow">
                                        <div class="inner">
                                            <h3>{{\App\Sale::whereDate('created_at',\Carbon\Carbon::today())->select(DB::raw('SUM(sell_price*quantity)-SUM(buy_price*quantity) as sell_price'))->first()->sell_price}}</h3>

                                            <p>Total Profit</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-dollar"></i>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Total Sell & Profit </h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="col-lg-6 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-aqua">
                                        <div class="inner">
                                            <h3>{{\App\Sale::all()->count()}}</h3>

                                            <p>Total Sell</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-dollar"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-yellow">
                                        <div class="inner">
                                            <h3>{{\App\Sale::select(DB::raw('SUM(sell_price*quantity)-SUM(buy_price*quantity) as sell_price'))->first()->sell_price}}</h3>

                                            <p>Total Profit</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-dollar"></i>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Last 30 days Profit </h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="col-lg-12 col-xs-12">
                                    <!-- small box -->
                                    <div id="chartdiv" style="height: 370px; max-width: 100%; margin: 0px auto;"></div>
                                </div>



                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>


                <!-- /.box -->

                <!-- DONUT CHART -->

            </div>

            <!-- /.col (LEFT) -->

            <!-- /.col (RIGHT) -->
        </div>



        <!-- /.row -->

    </section>
    @endsection
@section('script')

    <!-- jQuery 3 -->
    <script src="{{asset('shop')}}/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{asset('shop')}}/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{asset('shop')}}/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="{{asset('shop')}}/bower_components/raphael/raphael.min.js"></script>
    <script src="{{asset('shop')}}/bower_components/morris.js/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="{{asset('shop')}}/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="{{asset('shop')}}/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="{{asset('shop')}}/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{asset('shop')}}/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="{{asset('shop')}}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{asset('shop')}}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="{{asset('shop')}}/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{asset('shop')}}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="{{asset('shop')}}/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="{{asset('shop')}}/bower_components/fastclick/lib/fastclick.js"></script>
    <script src="{{asset('shop')}}/dist/js/pages/dashboard.js"></script>

    <script src="{{asset('js/core.js')}}"></script>
    <script src="{{asset('js/chart.js')}}"></script>
    <script src="{{asset('js/animated.js')}}"></script>
    <script>
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart);

        var chart_data = '';
        $.ajax({
            type: "GET",
            url: '{{route('dashboard')}}',
            dataType: "json",
            success: function (data) {

                 chart.data = data[0];

                var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                categoryAxis.dataFields.category = "country";
                categoryAxis.renderer.grid.template.location = 0;
                categoryAxis.renderer.minGridDistance = 30;
                categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
                    if (target.dataItem && target.dataItem.index & 2 == 2) {
                        return dy + 25;
                    }
                    return dy;
                });

                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                // Create series
                var series = chart.series.push(new am4charts.ColumnSeries());
                    series.dataFields.valueY = "visits";
                series.dataFields.categoryX = "country";
                series.name = "Visits";
                series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
                series.columns.template.fillOpacity = .8;

                var columnTemplate = series.columns.template;
                columnTemplate.strokeWidth = 2;
                columnTemplate.strokeOpacity = 1;

            }
        });

    </script>
@endsection