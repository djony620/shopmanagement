<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Setting::create(['slug'=>'shop_title','value'=>'Shop management']);
        \App\Setting::create(['slug'=>'logo','value'=>'logo.png']);
    }
}
