<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Product::create(['title'=>'product 1','photo'=>'p1.jpg','quantity'=>10,'buy_price'=>100,'sell_price'=>120,'discount'=>5,'discount_type'=>1,'stock'=>50,'product_code'=>'ab20']);
        \App\Product::create(['title'=>'product 2','photo'=>'p2.jpg','quantity'=>20,'buy_price'=>650,'sell_price'=>700,'discount'=>52,'discount_type'=>1,'stock'=>33,'product_code'=>'ab21']);
        \App\Product::create(['title'=>'product 3','photo'=>'p3.jpg','quantity'=>30,'buy_price'=>555,'sell_price'=>600,'discount'=>30,'discount_type'=>1,'stock'=>41,'product_code'=>'ab22']);
        \App\Product::create(['title'=>'product 4','photo'=>'p4.jpg','quantity'=>40,'buy_price'=>650,'sell_price'=>675,'discount'=>15,'discount_type'=>2,'stock'=>12,'product_code'=>'ab23']);
        \App\Product::create(['title'=>'product 5','photo'=>'p5.jpg','quantity'=>18,'buy_price'=>141,'sell_price'=>174,'discount'=>20,'discount_type'=>2,'stock'=>50,'product_code'=>'ab24']);
        \App\Product::create(['title'=>'product 6','photo'=>'p6.jpg','quantity'=>25,'buy_price'=>582,'sell_price'=>700,'discount'=>10,'discount_type'=>1,'stock'=>33,'product_code'=>'ab25']);
        \App\Product::create(['title'=>'product 7','photo'=>'p7.jpg','quantity'=>22,'buy_price'=>65,'sell_price'=>100,'discount'=>15,'discount_type'=>2,'stock'=>25,'product_code'=>'ab26']);
        \App\Product::create(['title'=>'product 8','photo'=>'p8.jpg','quantity'=>10,'buy_price'=>154,'sell_price'=>170,'discount'=>50,'discount_type'=>1,'stock'=>30,'product_code'=>'ab27']);
        \App\Product::create(['title'=>'product 9','photo'=>'p9.jpg','quantity'=>10,'buy_price'=>65,'sell_price'=>80,'discount'=>41,'discount_type'=>2,'stock'=>50,'product_code'=>'ab28']);
        \App\Product::create(['title'=>'product 10','photo'=>'p10.jpg','quantity'=>10,'buy_price'=>89,'sell_price'=>100,'discount'=>'25','discount_type'=>1,'stock'=>10,'product_code'=>'ab29']);
    }
}
