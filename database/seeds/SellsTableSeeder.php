<?php

use Illuminate\Database\Seeder;
use  \App\Sale;
class SellsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Sale::create(['sell_id'=>'1','product_id'=>1,'quantity'=>5,'discount'=>'0','buy_price'=>100,'sell_price'=>320,'created_at'=>'2018-11-20 19:23:16']);
        Sale::create(['sell_id'=>'1','product_id'=>2,'quantity'=>6,'discount'=>'0','buy_price'=>650,'sell_price'=>800,'created_at'=>'2018-11-20 19:23:16']);
        Sale::create(['sell_id'=>'1','product_id'=>3,'quantity'=>7,'discount'=>'0','buy_price'=>555,'sell_price'=>700,'created_at'=>'2018-11-20 19:23:16']);
        Sale::create(['sell_id'=>'1','product_id'=>4,'quantity'=>8,'discount'=>'0','buy_price'=>650,'sell_price'=>900,'created_at'=>'2018-11-20 19:23:16']);
        Sale::create(['sell_id'=>'1','product_id'=>5,'quantity'=>3,'discount'=>'0','buy_price'=>141,'sell_price'=>900,'created_at'=>'2018-11-20 19:23:16']);
        Sale::create(['sell_id'=>'1','product_id'=>6,'quantity'=>2,'discount'=>'0','buy_price'=>582,'sell_price'=>700,'created_at'=>'2018-11-20 19:23:16']);

        Sale::create(['sell_id'=>'2','product_id'=>6,'quantity'=>1,'discount'=>'0','buy_price'=>100,'sell_price'=>500,'created_at'=>'2018-11-21 19:23:16']);
        Sale::create(['sell_id'=>'2','product_id'=>1,'quantity'=>5,'discount'=>'0','buy_price'=>650,'sell_price'=>900,'created_at'=>'2018-11-21 19:23:16']);
        Sale::create(['sell_id'=>'2','product_id'=>3,'quantity'=>3,'discount'=>'0','buy_price'=>555,'sell_price'=>650,'created_at'=>'2018-11-21 19:23:16']);
        Sale::create(['sell_id'=>'2','product_id'=>4,'quantity'=>2,'discount'=>'0','buy_price'=>650,'sell_price'=>800,'created_at'=>'2018-11-21 19:23:16']);
        Sale::create(['sell_id'=>'2','product_id'=>3,'quantity'=>10,'discount'=>'0','buy_price'=>141,'sell_price'=>174,'created_at'=>'2018-11-21 19:23:16']);
        Sale::create(['sell_id'=>'2','product_id'=>5,'quantity'=>5,'discount'=>'0','buy_price'=>582,'sell_price'=>700,'created_at'=>'2018-11-21 19:23:16']);


        Sale::create(['sell_id'=>'3','product_id'=>2,'quantity'=>20,'discount'=>'0','buy_price'=>100,'sell_price'=>120,'created_at'=>'2018-11-20 19:23:16']);
        Sale::create(['sell_id'=>'3','product_id'=>3,'quantity'=>40,'discount'=>'0','buy_price'=>650,'sell_price'=>700,'created_at'=>'2018-11-20 19:23:16']);
        Sale::create(['sell_id'=>'3','product_id'=>1,'quantity'=>30,'discount'=>'0','buy_price'=>555,'sell_price'=>600,'created_at'=>'2018-11-20 19:23:16']);
        Sale::create(['sell_id'=>'3','product_id'=>1,'quantity'=>8,'discount'=>'0','buy_price'=>650,'sell_price'=>675,'created_at'=>'2018-11-20 19:23:16']);
        Sale::create(['sell_id'=>'3','product_id'=>2,'quantity'=>3,'discount'=>'0','buy_price'=>141,'sell_price'=>174,'created_at'=>'2018-11-20 19:23:16']);
        Sale::create(['sell_id'=>'3','product_id'=>8,'quantity'=>2,'discount'=>'0','buy_price'=>582,'sell_price'=>700,'created_at'=>'2018-11-20 19:23:16']);


        Sale::create(['sell_id'=>'4','product_id'=>1,'quantity'=>40,'discount'=>'0','buy_price'=>100,'sell_price'=>120,'created_at'=>'2018-11-13 19:23:16']);
        Sale::create(['sell_id'=>'4','product_id'=>2,'quantity'=>55,'discount'=>'0','buy_price'=>650,'sell_price'=>700,'created_at'=>'2018-11-13 19:23:16']);
        Sale::create(['sell_id'=>'4','product_id'=>5,'quantity'=>32,'discount'=>'0','buy_price'=>555,'sell_price'=>600,'created_at'=>'2018-11-13 19:23:16']);
        Sale::create(['sell_id'=>'4','product_id'=>7,'quantity'=>14,'discount'=>'0','buy_price'=>650,'sell_price'=>675,'created_at'=>'2018-11-13 19:23:16']);
        Sale::create(['sell_id'=>'4','product_id'=>1,'quantity'=>13,'discount'=>'0','buy_price'=>141,'sell_price'=>174,'created_at'=>'2018-11-13 19:23:16']);
        Sale::create(['sell_id'=>'4','product_id'=>8,'quantity'=>25,'discount'=>'0','buy_price'=>582,'sell_price'=>700,'created_at'=>'2018-11-13 19:23:16']);


        Sale::create(['sell_id'=>'5','product_id'=>2,'quantity'=>5,'discount'=>'0','buy_price'=>500,'sell_price'=>700,'created_at'=>'2018-11-12 19:23:16']);
        Sale::create(['sell_id'=>'5','product_id'=>3,'quantity'=>6,'discount'=>'0','buy_price'=>800,'sell_price'=>100,'created_at'=>'2018-11-12 19:23:16']);
        Sale::create(['sell_id'=>'5','product_id'=>1,'quantity'=>7,'discount'=>'0','buy_price'=>555,'sell_price'=>600,'created_at'=>'2018-11-12 19:23:16']);
        Sale::create(['sell_id'=>'5','product_id'=>8,'quantity'=>8,'discount'=>'0','buy_price'=>500,'sell_price'=>100,'created_at'=>'2018-11-12 19:23:16']);
        Sale::create(['sell_id'=>'5','product_id'=>2,'quantity'=>3,'discount'=>'0','buy_price'=>150,'sell_price'=>300,'created_at'=>'2018-11-12 19:23:16']);
        Sale::create(['sell_id'=>'5','product_id'=>1,'quantity'=>2,'discount'=>'0','buy_price'=>582,'sell_price'=>700,'created_at'=>'2018-11-12 19:23:16']);

        Sale::create(['sell_id'=>'6','product_id'=>5,'quantity'=>5,'discount'=>'0','buy_price'=>100,'sell_price'=>120,'created_at'=>'2018-11-14 19:23:16']);
        Sale::create(['sell_id'=>'6','product_id'=>6,'quantity'=>6,'discount'=>'0','buy_price'=>650,'sell_price'=>1000,'created_at'=>'2018-11-14 19:23:16']);
        Sale::create(['sell_id'=>'6','product_id'=>1,'quantity'=>7,'discount'=>'0','buy_price'=>555,'sell_price'=>1400,'created_at'=>'2018-11-14 19:23:16']);
        Sale::create(['sell_id'=>'6','product_id'=>4,'quantity'=>8,'discount'=>'0','buy_price'=>650,'sell_price'=>800,'created_at'=>'2018-11-14 19:23:16']);
        Sale::create(['sell_id'=>'6','product_id'=>5,'quantity'=>3,'discount'=>'0','buy_price'=>141,'sell_price'=>800,'created_at'=>'2018-11-14 19:23:16']);
        Sale::create(['sell_id'=>'6','product_id'=>4,'quantity'=>2,'discount'=>'0','buy_price'=>582,'sell_price'=>700,'created_at'=>'2018-11-14 19:23:16']);

        Sale::create(['sell_id'=>'7','product_id'=>5,'quantity'=>5,'discount'=>'0','buy_price'=>100,'sell_price'=>350,'created_at'=>'2018-11-15 19:23:16']);
        Sale::create(['sell_id'=>'7','product_id'=>1,'quantity'=>6,'discount'=>'0','buy_price'=>650,'sell_price'=>700,'created_at'=>'2018-11-15 19:23:16']);
        Sale::create(['sell_id'=>'7','product_id'=>3,'quantity'=>7,'discount'=>'0','buy_price'=>555,'sell_price'=>600,'created_at'=>'2018-11-15 19:23:16']);
        Sale::create(['sell_id'=>'7','product_id'=>4,'quantity'=>8,'discount'=>'0','buy_price'=>650,'sell_price'=>1000,'created_at'=>'2018-11-15 19:23:16']);
        Sale::create(['sell_id'=>'7','product_id'=>5,'quantity'=>3,'discount'=>'0','buy_price'=>141,'sell_price'=>174,'created_at'=>'2018-11-15 19:23:16']);
        Sale::create(['sell_id'=>'7','product_id'=>8,'quantity'=>2,'discount'=>'0','buy_price'=>582,'sell_price'=>582,'created_at'=>'2018-11-15 19:23:16']);

        Sale::create(['sell_id'=>'8','product_id'=>5,'quantity'=>5,'discount'=>'0','buy_price'=>100,'sell_price'=>120,'created_at'=>'2018-11-16 19:23:16']);
        Sale::create(['sell_id'=>'8','product_id'=>2,'quantity'=>6,'discount'=>'0','buy_price'=>650,'sell_price'=>700,'created_at'=>'2018-11-16 19:23:16']);
        Sale::create(['sell_id'=>'8','product_id'=>3,'quantity'=>7,'discount'=>'0','buy_price'=>555,'sell_price'=>600,'created_at'=>'2018-11-16 19:23:16']);
        Sale::create(['sell_id'=>'8','product_id'=>4,'quantity'=>8,'discount'=>'0','buy_price'=>650,'sell_price'=>675,'created_at'=>'2018-11-16 19:23:16']);
        Sale::create(['sell_id'=>'8','product_id'=>2,'quantity'=>3,'discount'=>'0','buy_price'=>141,'sell_price'=>174,'created_at'=>'2018-11-16 19:23:16']);
        Sale::create(['sell_id'=>'8','product_id'=>1,'quantity'=>2,'discount'=>'0','buy_price'=>582,'sell_price'=>700,'created_at'=>'2018-11-16 19:23:16']);

        Sale::create(['sell_id'=>'9','product_id'=>5,'quantity'=>5,'discount'=>'0','buy_price'=>100,'sell_price'=>300,'created_at'=>'2018-11-17 19:23:16']);
        Sale::create(['sell_id'=>'9','product_id'=>2,'quantity'=>6,'discount'=>'0','buy_price'=>650,'sell_price'=>800,'created_at'=>'2018-11-17 19:23:16']);
        Sale::create(['sell_id'=>'9','product_id'=>3,'quantity'=>7,'discount'=>'0','buy_price'=>555,'sell_price'=>300,'created_at'=>'2018-11-17 19:23:16']);
        Sale::create(['sell_id'=>'9','product_id'=>4,'quantity'=>8,'discount'=>'0','buy_price'=>650,'sell_price'=>920,'created_at'=>'2018-11-17 19:23:16']);
        Sale::create(['sell_id'=>'9','product_id'=>5,'quantity'=>3,'discount'=>'0','buy_price'=>141,'sell_price'=>230,'created_at'=>'2018-11-17 19:23:16']);
        Sale::create(['sell_id'=>'9','product_id'=>1,'quantity'=>2,'discount'=>'0','buy_price'=>582,'sell_price'=>700,'created_at'=>'2018-11-17 19:23:16']);
    }
}
